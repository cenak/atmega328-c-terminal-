#include <avr/io.h>
#define F_CPU 20000000UL
#include <util/delay.h>
#include <avr/interrupt.h>

#define BAUD 9600
#define UBBRValue F_CPU/BAUD/16-1


#define DEBUG
//#define RELEASE
//#define WINDOWS
//#define OLD_COMMAND_STRUCTURE


void USART_init( uint8_t UBBR_val )
{

    UBRR0H = (uint8_t) (UBBR_val >> 8);
    UBRR0L = (uint8_t) UBBR_val;
    UCSR0B = (1 << RXEN0) | (1 << TXEN0);
    UCSR0C = (1 << USBS0) | (3 << UCSZ00);
}

void ADC_init ()
{
	ADCSRA |= (1 << ADEN) | (1 << ADPS1) | (1 << ADPS0);
	ADMUX |= (0 << REFS1) | (1 << REFS0) | (0 << MUX0) | (1 << MUX1) | (1 << MUX2) | (0 << MUX3);
}

//This function work
void USART_transmit(uint8_t transmitedData)
{
    while (!(UCSR0A & (1 << UDRE0)));
    UDR0 = transmitedData;
}

//This function work
void USART_transmit_string(char* transmitedString)
{
    while (!(UCSR0A & (1 << UDRE0)));
	uint8_t i = 0;
	
	while(transmitedString[i] != '\0' || transmitedString[i] != 0)
		USART_transmit((uint8_t)transmitedString[i++]);
}

//This function work
uint8_t USART_Receive()
{
	UCSR0A &= 0x7F;
	
    while(!(UCSR0A & (1 << RXC0)))
	{
		CheckSensorAsync();
		_delay_ms(500);
	}
	
    return UDR0;
}

//This function work (Not ideally, but work)
char* USART_Receive_String()
{
	UCSR0A &= 0x7F;
	char receivedString[20];
	uint8_t i = 0;
	
	for(i = 0; i < 20; i++)
	{
		receivedString[i] = (char)USART_Receive();
		
		if(receivedString[i] == '\0')
			break;
		
		if(receivedString[i] < 32 || receivedString[i] > 126)
			break;
	}
	receivedString[i] = '\0';
	
	USART_transmit(4);
	
	return receivedString;
}

//This function work
void OpenGate(uint8_t gateNumber)
{
	switch(gateNumber)
	{
		case 1+48:
			PORTD |= 0x4;
			break;
		
		case 2+48:
			PORTD |= 0x8;
			break;
		
		case 3+48:
			PORTD |= 0x10;
			break;
		
		case 4+48:
			PORTD |= 0x20;
			break;
		
		case 5+48:
			PORTD |= 0x40;
			break;
			
		case 6+48:
			PORTD |= 0x80;
			break;
		
		default:
			USART_transmit_string("Error: Wrong gate number\n");
			return;
			break;
	}
}

//This function work
void CloseGate(uint8_t gateNumber)
{
	switch(gateNumber)
	{
		case 1+48:
			PORTD &= 0xFB;
			break;
		
		case 2+48:
			PORTD &= 0xF7;
			break;
		
		case 3+48:
			PORTD &= 0xEF;
			break;
		
		case 4+48:
			PORTD &= 0xDF;
			break;
		
		case 5+48:
			PORTD &= 0xBF;
			break;
			
		case 6+48:
			PORTD &= 0x7F;
			break;
		
		default:
			USART_transmit_string("Error: Wrong gate number\n");
			return;
			break;
	}
}

uint8_t CheckSensor()
{
	ADCSRA |= (1 << ADSC);
	
	while(!(ADCSRA & (1 << ADIF)));
	
	if(ADCH >= 2)
		return 0 + 48;
	else
		return 1 + 48;
}

uint8_t ADCDataPrevious = 2;

void CheckSensorAsync()
{
	
	uint8_t ADCData = CheckSensor();
	
	if(ADCDataPrevious != ADCData)
	{
		USART_transmit(ADCData);
		ADCDataPrevious = ADCData;
	}
	
}

uint8_t ADCData = 0;
uint8_t ADCDataH = 0;
uint8_t ADCDataL = 0;

void StateHandlerOld()
{
	uint8_t channelNumber = USART_Receive();
	
	#ifdef WINDOWS
		USART_Receive();
		USART_Receive();
	#endif
	
	#ifdef DEBUG
		USART_transmit_string("\nState Handler\n\n");
		
		USART_transmit_string("Channel number = ");
		USART_transmit(channelNumber);
		USART_transmit('\n');
	#endif
	
	uint8_t stateOfChannel = 0;
	
	switch(channelNumber)
	{
		case 1+48:
			stateOfChannel = ((PORTD & 4) >> 2) + 48;
			
			break;
			
		case 2+48:
			stateOfChannel = ((PORTD & 8) >> 3) + 48;
			
			break;
			
		case 3+48:
			stateOfChannel = ((PORTD & 16) >> 4) + 48;
			
			break;
			
		case 4+48:
			stateOfChannel = ((PORTD & 32) >> 5) + 48;
			
			break;
		
		case 5+48:
			stateOfChannel = ((PORTD & 64) >> 6) + 48;
			
			break;
		
		//ADC
		case 6+48:
			ADCData = CheckSensor();

			#ifdef DEBUG
				USART_transmit_string("ADC = ");
			#endif
			
			USART_transmit(ADCData);
			
			return;
			
			break;
			
		default:
			USART_transmit_string("Error: wrong channel number\n");
			return;
			break;
	}
	
	USART_transmit(stateOfChannel);
}

void TurnsHandlerOld()
{	
	uint8_t channelNumber = USART_Receive();
	
	#ifdef DEBUG
		USART_transmit_string("\nTurns Handler\n\n");
		USART_transmit_string("Channel number = ");
		USART_transmit(channelNumber);
		USART_transmit('\n');
	#endif
	
	#ifdef WINDOWS
		USART_Receive();
		USART_Receive();
	#endif
	
	uint8_t onOff = USART_Receive();
	
	#ifdef DEBUG
		USART_transmit_string("Switch to on or off = ");
		USART_transmit(onOff);
		USART_transmit('\n');
	#endif
	
	#ifdef WINDOWS
		USART_Receive();
		USART_Receive();
	#endif

	if(onOff == 1+48) //On
		OpenGate(channelNumber);
	
	else if (onOff == 0+48) //Off
		CloseGate(channelNumber);
	
	else USART_transmit_string("Error: Wrong argument\n");
	
}

//Work
void CheckUSBForCommand()
{
	#ifdef OLD_COMMAND_STRUCTURE
	
		uint8_t command = USART_Receive();
		
		#ifdef DEBUG
			USART_transmit_string("Command = ");
			USART_transmit(command);
			USART_transmit('\n');
		#endif
		
		#ifdef WINDOWS
			USART_Receive();
			USART_Receive();
		#endif
		
		if(command == 1+48)
			TurnsHandlerOld();
		
		else if(command == 2+48)
			StateHandlerOld();
		
		else USART_transmit_string("Error: Wrong command\n");
	
	#endif
	
	#ifndef OLD_COMMAND_STRUCTURE
		char* commandStr = USART_Receive_String();
		
		#ifdef DEBUG
			USART_transmit_string("Command = ");
			USART_transmit_string(commandStr);
			USART_transmit('\n');
		#endif
		
		uint8_t i = 0;
		
		uint8_t stateSelection = 0;
		//
		// 0 - task selection (1 turn, 2 state)
		// 1 - channel selection
		// 2 - argument taking
		
		uint8_t task = 0;
		uint8_t channels[6] = {0};
		uint8_t argument = 0;
		
		char channelsStr[12] = {0};
		uint8_t j = 0;
		
		uint8_t checkingFinished = 0;
		//
		// 0 - Checking is continuing
		// 1 - Checking is finished
		
		for(i = 0; i < 20; i++)
		{
			
			if(commandStr[i] == '\0' || commandStr[i] < 32 || commandStr[i] > 126)
				break;
			
			if(commandStr[i] == ',')
			{
				stateSelection++;
				continue;
			}
			
			switch(stateSelection)
			{
				case 0:
					task = commandStr[i];
					
					#ifdef DEBUG
						USART_transmit('\n');
						USART_transmit_string("Task = ");
						USART_transmit(task);
						USART_transmit('\n');
					#endif
					
					break;
				
				case 1:
					
					channelsStr[j] =  commandStr[i];
					j++;
					
					break;
				
				case 2:
					argument = commandStr[i];
					checkingFinished = 1;
					
					#ifdef DEBUG
						USART_transmit('\n');
						USART_transmit_string("Argument = ");
						USART_transmit(argument);
						USART_transmit('\n');
					#endif
					
					break;
					
				default:
					USART_transmit_string("Error: stateSelection > 2\n");
					break;
			}
		}
		
		if(checkingFinished)
		{
			channelsStr[j + 1] = 'A';
			
			GetChannels(channelsStr, channels);
			
			#ifdef DEBUG
				USART_transmit('\n');
				USART_transmit_string("Channels = ");
				USART_transmit(channels[0]+48);
				USART_transmit(' ');
				USART_transmit(channels[1]+48);
				USART_transmit(' ');
				USART_transmit(channels[2]+48);
				USART_transmit(' ');
				USART_transmit(channels[3]+48);
				USART_transmit(' ');
				USART_transmit(channels[4]+48);
				USART_transmit(' ');
				USART_transmit(channels[5]+48);
				USART_transmit('\n');
			#endif
			
			CommandHandler(task, channels, argument);
		}
			
		
	#endif
}

void GetChannels(char* channelsStr, uint8_t* channels)
{
	uint8_t i = 0;
	uint8_t j = 0;
	uint8_t l = 0;
	
	while(channelsStr[i] != 'A')
	{
		if(channelsStr[i] > 47 && channelsStr[i] < 58)
		{
			channels[j] = channelsStr[i] - 48;
			j++;
		}
		
		if(channelsStr[i] == '&')
		{
			channels[j] = channelsStr[i + 1] - 48;
			i++;
			j++;
		}
		
		if(channelsStr[i] == '-')
		{
			for(l = (channelsStr[i - 1] - 48) + 1; l <= (channelsStr[i + 1] - 48); l++)
			{
				channels[j] = l;
				j++;
			}
			i++;
		}
		
		if(channelsStr[i] == 'A')
			break;
		
		i++;
	}
}

void CommandHandler(uint8_t task, uint8_t* channels, uint8_t argument)
{
	if(task == 1 + 48)
		TurnsHandler(channels, argument);
	
	else if(task == 2 + 48)
		StateHandler(channels, argument);
	
	else
		USART_transmit_string("Error: wrong command number\n");
}

void TurnsHandler(uint8_t* channels, uint8_t argument)
{
	uint8_t i = 0;
	if(argument == 1 + 48)
	{
		while(channels[i] != 0)
		{
			OpenGate(channels[i] + 48);
			i++;
		}
	}
	
	else if(argument == 0 + 48)
	{
		while(channels[i] != 0)
		{
			CloseGate(channels[i] + 48);
			i++;
		}
	}
	
	else
		USART_transmit_string("Error: wrong argument\n");
	
	USART_transmit(1); //This mean thats all is ok
}

void StateHandler(uint8_t* channels, uint8_t argument)
{
	uint8_t stateOfChannel = 0;
	
	switch(channels[0])
	{
		case 1:
			stateOfChannel = ((PORTD & 4) >> 2) + 48;
			
			break;
			
		case 2:
			stateOfChannel = ((PORTD & 8) >> 3) + 48;
			
			break;
			
		case 3:
			stateOfChannel = ((PORTD & 16) >> 4) + 48;
			
			break;
			
		case 4:
			stateOfChannel = ((PORTD & 32) >> 5) + 48;
			
			break;
		
		case 5:
			stateOfChannel = ((PORTD & 64) >> 6) + 48;
			
			break;
		
		//ADC
		case 6:
			ADCData = CheckSensor();

			#ifdef DEBUG
				USART_transmit_string("ADC = ");
			#endif
			
			USART_transmit(ADCData);
			
			return;
			
			break;
			
		default:
			USART_transmit_string("Error: wrong channel number\n");
			return;
			break;
	}
	
	#ifdef DEBUG
		USART_transmit_string("stateOfChannel = ");
	#endif
	
	USART_transmit(stateOfChannel);
}



int main(void)
{
    cli();

    USART_init( UBBRValue );
    
	ADC_init();
	
	DDRD = 0x3F;
	PORTD &= 0x3;
	
    sei();
	
    while (1)
    {
		CheckSensorAsync();
		CheckUSBForCommand();
    }
}
