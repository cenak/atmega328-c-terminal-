#include <avr/io.h>
#define F_CPU 20000000UL
#include <util/delay.h>
//#include <math.h>
#include <avr/interrupt.h>

#define sbi(port_name, pin_number)   (port_name |= 1<<pin_number)
#define cbi(port_name, pin_number)   ((port_name) &= (uint8_t)~(1 << pin_number))
#define BAUD 9600
#define UBBRValue F_CPU/BAUD/16-1
//#define dig_size 4

//#define TRIG_PIN 1
//#define ECHO_PIN 0
//#define Echo_div 58



void USART_init( uint8_t UBBR_val )
{

    UBRR0H = (uint8_t) (UBBR_val >> 8);
    UBRR0L = (uint8_t) UBBR_val;
    UCSR0B = (1 << RXEN0) | (1 << TXEN0);
    UCSR0C = (1 << USBS0) | (3 << UCSZ00);
}
/*
void Timer_init()
{
	sbi(SREG, 7);

	TCCR1A = 0;

	sbi(TCCR1B ,ICNC1);

	cbi(TCCR1B ,ICES1);

	cbi(TCCR1B ,WGM13);
	cbi(TCCR1B ,WGM12);

	cbi(TCCR1B ,CS12);
	cbi(TCCR1B ,CS11);
	sbi(TCCR1B ,CS10);
}
*/
/*
void Port_init()
{


	sbi(DDRB, TRIG_PIN);
	cbi(PORTB, TRIG_PIN); //PORTB1 out 1

	cbi(DDRB, ECHO_PIN); //PORTB0 in

}
*/
/*
void Triger()
{
	sbi(PORTB, TRIG_PIN);
	_delay_us(10);
	cbi(PORTB, TRIG_PIN);
}

void Echo()
{
	Triger();
	int Echo_time = 0 ;

	//while (!(UCSR0A & (1 << UDRE0)));
    //UDR0 = PINB;

	while ( bit_is_clear(PINB,ECHO_PIN));
	
		TIFR1 = (1<<ICF1); 
		while ( bit_is_clear(TIFR1, ICF1));
		Echo_time = TCNT1;
		int Echo_distance = Echo_time / Echo_div;
		while (!(UCSR0A & (1 << UDRE0)));
    	UDR0 = Echo_distance;
    	while (!(UCSR0A & (1 << UDRE0)));
    	UDR0 = (Echo_distance>>8);
}	
	
	
	//int Echo_distance = Echo_time / Echo_div;
	//return 1;
*/




void USART_transmit(uint8_t Echo_distance)
{

    while (!(UCSR0A & (1 << UDRE0)));
    UDR0 = Echo_distance;
  
}

uint8_t i = 0;

int main(void)
{
    cli();

    //Port_init();
    //Timer_init();
    USART_init( UBBRValue );
    
    sei();
    while (1)
    {
            //Echo();
           // _delay_ms(1000);
            // Echo_distance = 0;
			//int Echo_distance = Echo();
            USART_transmit(i);
            _delay_ms(1000);
            //_delay_ms(500);
            i++;
    }
}
